/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TaskManager;

import TaskManager.Task.Priority;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author Lukasz
 */
public class TaskManager {
    
    private Queue<Task> taskQueue;

    public TaskManager() {
        taskQueue = new PriorityQueue<Task>(new QueueComparator());
    }
    
    public static void main(String[] args) {
        TaskManager taskManager = new TaskManager();
        
        taskManager.taskQueue.offer(new Task("zakupy", "zrobic zakupy", Priority.HIGH));
        taskManager.taskQueue.offer(new Task("java", "programowac", Priority.MODERATE));
        taskManager.taskQueue.offer(new Task("C++", "programowac", Priority.HIGH));
        taskManager.taskQueue.offer(new Task("C#", "programowac", Priority.LOW));
        taskManager.taskQueue.offer(new Task("javaScript", "programowac", Priority.HIGH));
        taskManager.taskQueue.offer(new Task("wycieczka", "gorska wedrowka", Priority.MODERATE));
        
        taskManager.taskQueue.forEach((item) -> System.out.println(item.toString()));
        System.out.println("Pobrano:");
        System.out.println(taskManager.taskQueue.poll().toString());
        System.out.println(taskManager.taskQueue.poll().toString());
        System.out.println(taskManager.taskQueue.poll().toString());
        System.out.println(taskManager.taskQueue.poll().toString());
    }
}
