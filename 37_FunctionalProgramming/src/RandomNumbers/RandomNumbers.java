/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 *
 * @author Lukasz
 */
public class RandomNumbers {
    
    private List<Integer> numbers;

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public RandomNumbers() {
        this.numbers = new ArrayList<Integer>();
    }
    
    public <T> void supplyNumbers(List<T> list, Supplier<T> supplier){
        for (int i = 0  ; i < 10 ; i++){
            list.add(supplier.get());
        }
    }
    
    public <T> void showNumbers (List<T> list, Consumer<T> consume){
        for (T number : list){
            consume.accept(number);
        }
    }
    
    public <T> void removeDividedBy2 (List<T> list, Predicate<T> predicate){
        Iterator iter = list.iterator();
        while (iter.hasNext()){
            if (predicate.test((T)iter.next())){
                iter.remove();
            }
        }
    }
    
}
