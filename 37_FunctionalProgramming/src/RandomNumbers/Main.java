/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;

/**
 *
 * @author Lukasz
 */
public class Main {

    public static void main(String[] args) {
        RandomNumbers randomNumbers = new RandomNumbers();
        randomNumbers.supplyNumbers(randomNumbers.getNumbers(), () -> {
                Random rand = new Random();
                return rand.nextInt(100);
        });
        
        randomNumbers.showNumbers(randomNumbers.getNumbers(), (number) -> {
                System.out.print(number.toString() + ",");
        });
        System.out.println();
        randomNumbers.removeDividedBy2(randomNumbers.getNumbers(), (number) -> {
                return (number%2 ==0);
        });
        
        System.out.println("Usunieto liczby podzielne przez 2:");
        randomNumbers.showNumbers(randomNumbers.getNumbers(), (number) -> {
                System.out.print(number.toString() + ",");
        });
        
        System.out.println();
    }
    
}
