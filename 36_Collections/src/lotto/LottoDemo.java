/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lotto;

import java.util.Arrays;

/**
 *
 * @author Lukasz
 */
public class LottoDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Lotto lotto = new Lotto();
        lotto.generate();
        System.out.println(lotto.toString());
        lotto.randomize();
        System.out.println(lotto.toString());
        lotto.checkResult(Arrays.asList(1,34,15,10,21,2));
    }
    
}
