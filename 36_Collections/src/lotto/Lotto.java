/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lotto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Lukasz
 */
public class Lotto {
    
    private List<Integer> lottoNumbers;
    private List<Integer> winningNumbers;
    
    public Lotto() {
        this.lottoNumbers = new ArrayList<Integer>();
        this.winningNumbers = new ArrayList<Integer>();
    }

    public void generate(){
        for (int i = 1 ; i < 50 ; i++){
            lottoNumbers.add(i);
        }
    }
    
    public void randomize(){
        Collections.shuffle(lottoNumbers);
        winningNumbers = lottoNumbers.subList(0, 5);
    }
    
    public void checkResult(List<Integer> myNumbers){
        int match = 0;
        for (Integer number: myNumbers){
                      if (winningNumbers.contains(number)) match++;  
        }
        System.out.println("Trafiles "+match+" liczb");
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder();
        lottoNumbers.forEach((item) -> {returnString.append(item.toString()).append(",");});
        return returnString.toString();
    }   
    
}

