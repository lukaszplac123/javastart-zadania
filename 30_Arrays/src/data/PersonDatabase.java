/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Arrays;

/**
 *
 * @author Lukasz
 */
public class PersonDatabase {
    
    private Person[] people;
  //  private int currentSize;

    public Person[] getPeople() {
        return people;
    }

    public void setPeople(Person[] people) {
        this.people = people;
    }
  

    public PersonDatabase() {
       // this.currentSize = 1;
        this.people = new Person[1];
    }
    
    public void add(Person person){
        int i = 0;
        while (people[i] != null) {
            i++;
            if (i >= people.length) {
                    people = Arrays.copyOf(people, people.length * 2);
                    break;
            }
        }
        people[i] = person;
    }
    
    public void remove(Person person){
        int indexToRemove = 0;
        Person[] copy = new Person[people.length];
        System.arraycopy(people, 0, copy, 0, people.length);
        Person[] peopleAfterIndex = null;
        for (int i = 0 ; i <  this.size() ; i++){
            if (people[i].equals(person)){
                peopleAfterIndex = Arrays.copyOfRange(copy, i+1 , people.length);
                indexToRemove = i;
            }
        }
        System.arraycopy(peopleAfterIndex, 0, people, indexToRemove, people.length - indexToRemove - 1);
    }
    
    public Person get(int index){
        return people[index];
    }
    
    public int size(){
        int size = 0;
        for (Person item : people){
            if (item != null){
                size++;
            } else break;
        }
        return size;
    }
    
}
