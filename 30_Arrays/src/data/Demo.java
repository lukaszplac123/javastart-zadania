/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 * Zabawy z klasą Arrays
 * @author Lukasz
 */
public class Demo {
    public static void main(String[] args) {
        PersonDatabase pdb = new PersonDatabase();
        Person p2 = new Person("12345","Wojtek", "Baba");
        pdb.add(new Person("123","Lukasz", "Plac"));
        pdb.add(new Person("1234","Ania", "Mruk"));
        pdb.add(p2);
        pdb.add(new Person("123456","Bartek", "Gil"));
        pdb.add(new Person("1234567","Wojtek", "Gil"));
        
        for (Person person : pdb.getPeople()){
            if (person != null) System.out.println(person.toString());
        }
        System.out.println("Size:"+pdb.size());
        System.out.println("Array length:"+pdb.getPeople().length);
        
        //wurzucam z tablicy Wojtka Baba
        pdb.remove(p2);
        
        for (Person person : pdb.getPeople()){
            if (person != null) System.out.println(person.toString());
        }
        
        System.out.println("Size:"+pdb.size());
        System.out.println("Array length:"+pdb.getPeople().length);
    }
}
