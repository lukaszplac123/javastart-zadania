/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg31_compares;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import java.util.stream.DoubleStream;

/**
 *
 * @author w6350
 */
public class Main {
    
    public static void main(String[] args) {
        final int size = 50;
        Integer[] array = generateRandomArray(size);
        System.out.println(Arrays.toString(array));
        
        System.out.println("Posortowana rosnaco:");
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        
        System.out.println("Posortowana malejaco:");
        Arrays.sort(array, new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                return -(o1.compareTo(o2));
            }
        });
        System.out.println(Arrays.toString(array));
    }
    
    private static Integer[] generateRandomArray(int size){
        Random rand = new Random();
        Integer[] temp = new Integer[size];
        for (int i = 0 ; i < size ; i++) temp[i] = rand.nextInt(1000);
        return temp;
    }
}
