/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Lukasz
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Franek","Jozek","Anka","Jola","Zdzisiek");
        
        
        //zamiast definiowania komparatora mozna przekazac referencje do metody
        Collections.sort(names, String::compareToIgnoreCase);
        
        for(String name: names) {
            System.out.println(name);
        }
        
    }
    
}
