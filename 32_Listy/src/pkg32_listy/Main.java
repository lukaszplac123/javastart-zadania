/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg32_listy;

import static java.lang.System.in;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Lukasz
 */
public class Main {

    private Scanner scanner;
    private List<Integer> intList;
    private int sum;
    private int last;
    
    public Main(){
        scanner = new Scanner(in);
        intList = new ArrayList<>();
        sum = 0;
        last=0;
    }
    public static void main(String[] args) {
        
        Main app = new Main();
        String data = "0";
        
        while (!data.equals("exit")){
            data = app.readData();
            if (!data.equals("exit")){
                try {
                    app.intList.add(Integer.parseInt(data));
                    }catch (NumberFormatException e){
                    System.out.println("Zly format. Sprobuj jeszcze raz");
                    }
            }
        }
        
        app.intList.forEach((item) -> {
                if (app.last != app.intList.size()-1){
                    System.out.print(item.toString() + ",");                    
                } else {
                    System.out.print(item.toString());
                }
                app.last++;
        });
        app.last = 0;
        System.out.println();
        app.intList.forEach((item) -> {
                if (app.last != app.intList.size()-1){
                    System.out.print(item.toString() + "+");                    
                } else {
                    System.out.print(item.toString() + "=");
                }
                app.sum += item;
                app.last++;
        });
        System.out.print(app.sum);
    }
    
    private String readData(){
        return scanner.nextLine();
    }
}
