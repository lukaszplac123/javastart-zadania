/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg33_zbiory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author Lukasz
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        File file  = new File("src/resources/namespl.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        NamesSet names = new NamesSet();
        
        String line;
        while ((line = reader.readLine()) != null){
            names.getNames().add(line);
        }
      
        names.getNames().forEach((name) -> System.out.println(name));
        System.out.println("Ilosc imion w ziorze:" + names.getNames().size());
        reader.close();
    }
    
}
