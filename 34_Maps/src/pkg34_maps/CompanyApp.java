/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg34_maps;

import java.util.Scanner;

/**
 *
 * @author w6350
 */
public class CompanyApp {

    private static CompanyApp app;
    private Scanner scanner;
    private Company company;

    private CompanyApp() {
        this.scanner = new Scanner(System.in);
        company = new Company();
    }
    
    public static CompanyApp getInstance(){
        if (app == null){
            app = new CompanyApp();
        }
        return app;
    }
    
    public static void main(String[] args) {
        
        CompanyApp app = CompanyApp.getInstance();
        app.readData();
        System.out.println("Wyszukaj po nazwisku:");
        String surname = (new Scanner(System.in)).nextLine();
        System.out.println(app.company.getEmployees().get(surname).toString());
    }
    
    private void readData(){
      String decision = "";
      while (!decision.equals("N")){
        System.out.println("Podaj imie pracownika");
        String name = app.scanner.nextLine();
        System.out.println("Podaj nazwisko pracownika");
        String surname = app.scanner.nextLine();
        System.out.println("Podaj wynagrodzenie");
        double salary = app.scanner.nextDouble();
        System.out.println("Dodac nastepnego (T/N)");
        decision = app.scanner.nextLine();
        decision = app.scanner.nextLine();
        company.addEmployee(surname, new Employee(name, surname, salary));
      }
    }
}
