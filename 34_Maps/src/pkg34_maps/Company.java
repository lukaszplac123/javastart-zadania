/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg34_maps;

import java.util.HashMap;


public class Company {
   
    HashMap<String, Employee> employees;

    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(HashMap<String, Employee> employees) {
        this.employees = employees;
    }

    public Company() {
        employees = new HashMap<>();
    }
    
    public void addEmployee(String surname, Employee employee){
        this.employees.put(surname, employee);
    }
    
}
