/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Lukasz
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //1. metoda Stream.generate() - generuje strumien
        //2. metoda filter() filtruje strumien wg. podanych predykatow (funkcji testujacych)
        //3. metoda limit(10) ogranicz liczbe elementow w strumieniu do 10
        //4. metoda map wykonuje dana funkcje dla kazdego elementu strumienia (tutaj mnozy go przez 3)
        //5. metoda collect przepisuje otrzymany strumien do listy
        List<Integer> list = Stream.iterate(0, x -> x+1)
                                    .filter((x) -> ((x>100)&&(x<1000)&&(x%5 == 0)))
                                    .limit(10)
                                    .map(x -> x * 3)
                                    .collect(Collectors.toList());
                   
        //poprez przzekazanie referncji metody println wyswietlamy liste na konsoli
        //metoda ta jest konsumentem kazego elementu
        list.forEach(System.out::println);
    }
}
